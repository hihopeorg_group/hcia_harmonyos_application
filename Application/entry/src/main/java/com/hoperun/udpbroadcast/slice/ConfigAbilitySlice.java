package com.hoperun.udpbroadcast.slice;

import com.hoperun.udpbroadcast.ActivityTools;
import com.hoperun.udpbroadcast.Log;
import com.hoperun.udpbroadcast.ResourceTable;
import com.hoperun.udpbroadcast.LightAbility;
import com.hoperun.udpbroadcast.broadcast.BroadcastUtils;
import com.hoperun.udpbroadcast.broadcast.Config;
import com.hoperun.udpbroadcast.broadcast.UdpBroadcastClient;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.utils.zson.ZSONObject;

import java.net.SocketException;

public class ConfigAbilitySlice extends AbilitySlice {

    private String broadcastIp = "192.168.40.255";//"255.255.255.255";       //广播范围
    private int broadcastPort = 5054;     //端口
    private int receivePort = 5055;

    TextField sendPortEdit, ssidEdit, passwordEdit;
    Text textIp;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_first_config);

        textIp = (Text) findComponentById(ResourceTable.Id_text_ip);
        sendPortEdit = (TextField) findComponentById(ResourceTable.Id_sendPortEdit);
        ssidEdit = (TextField) findComponentById(ResourceTable.Id_ssidEdit);
        passwordEdit = (TextField) findComponentById(ResourceTable.Id_passwordEdit);
        passwordEdit.setText("");
        findComponentById(ResourceTable.Id_nextBtn).setClickedListener(this::onClickNext);
        findComponentById(ResourceTable.Id_sendBtn).setClickedListener(this::onClickSend);

        sendPortEdit.setText(String.valueOf(broadcastPort));
        try {
            broadcastIp = BroadcastUtils.getPhoneBroadcast();
            textIp.setText(broadcastIp);
        } catch (SocketException e) {
            e.printStackTrace();
            Log.w("get phone broadcast", e);
        }

        String sSid = BroadcastUtils.getSSid(getContext());
        ssidEdit.setText(sSid);
        passwordEdit.requestFocus();

        Window window = getWindow();
        window.setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);
    }

    private void onClickSend(Component component) {
        broadcastPort = Integer.parseInt(sendPortEdit.getText());
        sendNetworkInfo();
    }

    private void sendNetworkInfo() {
        ZSONObject json = new ZSONObject();
        json.put("hotspot_ssid", ssidEdit.getText());
        json.put("hotspot_psk", passwordEdit.getText());
        UdpBroadcastClient client = new UdpBroadcastClient();
        client.setAddress(broadcastIp, broadcastPort);
        client.send(json.toString());

    }

    private void onClickNext(Component component) {
        broadcastPort = Integer.parseInt(sendPortEdit.getText());
//        receivePort = Integer.parseInt(receivePortEdit.getText());

        Intent intent = new Intent()
                .setParam(Config.BROADCAST_CLIENT_IP, broadcastIp)
                .setParam(Config.BROADCAST_CLIENT_PORT, broadcastPort)
                .setParam(Config.BROADCAST_SERVER_PORT, receivePort)
                .setParam(Config.NETWORK_SSID, ssidEdit.getText())
                .setParam(Config.NETWORK_PWD, passwordEdit.getText());

        ActivityTools.startActivity(this.getAbility(), LightAbility.class, intent);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
