package com.hoperun.udpbroadcast;

import com.hoperun.udpbroadcast.slice.LightAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class LightAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(LightAbilitySlice.class.getName());
    }
}
