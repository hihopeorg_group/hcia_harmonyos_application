package com.hoperun.udpbroadcast.broadcast;

import com.hoperun.udpbroadcast.Log;
import ohos.agp.components.TextField;
import ohos.app.Context;
import ohos.wifi.WifiDevice;
import ohos.wifi.WifiLinkedInfo;

import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class BroadcastUtils {

    public static boolean isEmpty(String str){
        return str == null || str.length() == 0;
    }

    public static int parseInt(TextField textField){
        try {
            if(!isEmpty(textField.getText())){
                return Integer.parseInt(textField.getText());
            }else if(!isEmpty(textField.getHint())){
                return Integer.parseInt(textField.getHint());
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            Log.w("parseInt",e);
        }
        return 0;
    }

    public static float parseFloat(TextField textField){
        try {
            if(!isEmpty(textField.getText())){
                return Float.parseFloat(textField.getText());
            }else if(!isEmpty(textField.getHint())){
                return Float.parseFloat(textField.getHint());
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            Log.w("parseInt",e);
        }
        return 0;
    }

    /**
     * 获取当前移动设备的广播地址
     * 返回示例：192.168.11.255
     */
    public static String getPhoneBroadcast() throws SocketException {
        System.setProperty("java.net.preferIPv4Stack", "true");
        for (Enumeration<NetworkInterface> niEnum = NetworkInterface.getNetworkInterfaces(); niEnum.hasMoreElements(); ) {
            NetworkInterface ni = niEnum.nextElement();
            if (!ni.isLoopback()) {
                for (InterfaceAddress interfaceAddress : ni.getInterfaceAddresses()) {
                    if (interfaceAddress.getBroadcast() != null) {
                        String str = interfaceAddress.getBroadcast().toString().substring(1);
                        return str;
                    }
                }
            }
        }
        return "";
    }

    public static String getPhoneAddress() throws SocketException {
        System.setProperty("java.net.preferIPv4Stack", "true");
        for (Enumeration<NetworkInterface> niEnum = NetworkInterface.getNetworkInterfaces(); niEnum.hasMoreElements(); ) {
            NetworkInterface ni = niEnum.nextElement();
            if (!ni.isLoopback()) {
                for (InterfaceAddress interfaceAddress : ni.getInterfaceAddresses()) {
                    if (interfaceAddress.getAddress() != null) {
                        return interfaceAddress.getAddress().getHostAddress();
                    }
                }
            }
        }
        return "";
    }

    public static String getSSid(Context context){
        WifiDevice device = WifiDevice.getInstance(context);
        if(device.isWifiActive() && device.isConnected()){
            WifiLinkedInfo linkedInfo = device.getLinkedInfo().get();
            String ssid = linkedInfo.getSsid();
            return ssid;
//            Optional<IpInfo> ipInfo = device.getIpInfo();
//            int ipAddress = ipInfo.get().getIpAddress();
//            int gateway = ipInfo.get().getGateway();
        }

        return "";
    }
}
