/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hoperun.udpbroadcast;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

public class ActivityTools {

    public static void startActivity(Ability ability, Class<? extends Ability> nextAbility, Intent intent) {
        Intent secondIntent = intent == null ? new Intent() : intent;
        // 指定待启动FA的bundleName和abilityName
        Intent.OperationBuilder builder = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(ability.getBundleName())
                .withAbilityName(nextAbility.getCanonicalName());
        secondIntent.setOperation(builder.build());
        if(intent != null && intent.getParams() != null){
            secondIntent.setParams(intent.getParams());
        }

        // 通过AbilitySlice的startAbility接口实现启动另一个页面
        ability.startAbility(secondIntent);

    }

    public static void changePage(AbilitySlice current, AbilitySlice next, Intent intent) {
        current.present(next, intent);
    }

    public static ComponentContainer createXmlComponent(Context context, int xmlId) {
        return (ComponentContainer) LayoutScatter.getInstance(context).parse(xmlId, null, false);
    }
}
