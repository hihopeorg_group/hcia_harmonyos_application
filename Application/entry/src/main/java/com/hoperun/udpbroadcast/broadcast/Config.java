package com.hoperun.udpbroadcast.broadcast;

import ohos.aafwk.content.Intent;

public class Config {
    public static final String BROADCAST_SERVER_PORT="BROADCAST_SERVER_PORT";
    public static final String BROADCAST_CLIENT_PORT="BROADCAST_CLIENT_PORT";
    public static final String BROADCAST_CLIENT_IP="BROADCAST_CLIENT_IP";
    public static final String NETWORK_SSID="NETWORK_SSID";
    public static final String NETWORK_PWD="NETWORK_PWD";

    private String broadcastIp;
    private int broadcastPort;
    private int broadcastServerPort;

    public static Config parseIntent(Intent intent){
        Config config = new Config();
        if(intent != null){
            config.broadcastIp = intent.getStringParam(BROADCAST_CLIENT_IP);
            config.broadcastPort = intent.getIntParam(BROADCAST_CLIENT_PORT,0);
            config.broadcastServerPort = intent.getIntParam(BROADCAST_SERVER_PORT,0);
        }
        return config;
    }

    public Intent createIntent(){
        Intent intent = new Intent()
                .setParam(Config.BROADCAST_CLIENT_IP, broadcastIp)
                .setParam(Config.BROADCAST_CLIENT_PORT, broadcastPort)
                .setParam(Config.BROADCAST_SERVER_PORT, broadcastServerPort);

        return intent;
    }

    public String getBroadcastIp() {
        return broadcastIp;
    }

    public void setBroadcastIp(String broadcastIp) {
        this.broadcastIp = broadcastIp;
    }

    public int getBroadcastPort() {
        return broadcastPort;
    }

    public void setBroadcastPort(int broadcastPort) {
        this.broadcastPort = broadcastPort;
    }

    public int getBroadcastServerPort() {
        return broadcastServerPort;
    }

    public void setBroadcastServerPort(int broadcastServerPort) {
        this.broadcastServerPort = broadcastServerPort;
    }
}
