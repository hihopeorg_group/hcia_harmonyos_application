package com.hoperun.udpbroadcast;

import com.hoperun.udpbroadcast.slice.ConfigAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ConfigAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ConfigAbilitySlice.class.getName());
    }
}
