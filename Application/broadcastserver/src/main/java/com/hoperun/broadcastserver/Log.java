/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.hoperun.broadcastserver;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class Log {
    private static final String MY_TAG = "MY_TAG " + Log.class.getPackage().getName();
    public static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, MY_TAG);

    public static void e(String tag, String log, Throwable e) {
        HiLog.error(LABEL, "Tag:%{public}s - Log:%{public}s - StackTrace:%{public}s", tag, log, HiLog.getStackTrace(e));
    }

    public static void e(String tag, Throwable e) {
        HiLog.error(LABEL, "Tag:%{public}s - StackTrace:%{public}s", tag, HiLog.getStackTrace(e));
    }

    public static void e(String tag, String log) {
        HiLog.error(LABEL, "Tag:%{public}s - Log:%{public}s", tag, log);
    }

    public static void w(String tag, Throwable e) {
        HiLog.warn(LABEL, "Tag:%{public}s - StackTrace:%{public}s", tag, HiLog.getStackTrace(e));
    }

    public static void w(String tag, String log) {
        HiLog.warn(LABEL, "Tag:%{public}s - Log:%{public}s", tag, log);
    }

    public static void d(String tag, String log) {
        HiLog.debug(LABEL, "Tag:%{public}s - Log:%{public}s", tag, log);
    }

    public static void i(String tag, String log) {
        HiLog.info(LABEL, "Tag:%{public}s - Log:%{public}s", tag, log);
    }

    public static void println(String tag, String log) {
        String str;
        if (tag.contains("%s")) {
            str = String.format(tag, log);
        } else {
            str = String.format("Tag:%s - Log:%s", tag, log);
        }
        debug(str);
    }

    public static void println(String string, Object... args) {
        String str = String.format(string, args);
        debug(str);
    }

    public static void debug(String log) {
        System.out.println(MY_TAG + "---" + log);
//        HiLog.warn(LABEL, log);
    }
}
